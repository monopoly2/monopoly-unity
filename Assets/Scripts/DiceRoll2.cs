﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRoll2 : MonoBehaviour
{
    public Rigidbody rb;   // connection au composante rigidBody pour savoir sa vélocité
    public bool hasLanded;
    public bool thrown;
    public Vector3 initPosition;
    public int diceValue;
    public DiceSide[] diceSides;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initPosition = transform.position;
        rb.useGravity = false;
    }

    private void Update()
    {
        RollDiceProtocal();
    }

    public void RollDiceProtocal()
    {
        // Quand on press Space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RollDice();
        }

        // Si le dee ne bouge pas , pas encore atterie et il etait lancer 
        if (rb.IsSleeping() && !hasLanded && thrown)
        {
            hasLanded = true;
            rb.useGravity = false;
            rb.isKinematic = true;
            SideValueCheck();
        }
        // si le dee ne bouge pas, il a atterie , mais on a reçu une erreur msg d'erreur
        else if (rb.IsSleeping() && hasLanded && diceValue == 0)
        {
            RollAgain();
        }

    }

    public void RollDice()
    {
        // J'ai lancer mon dee , je veux savoir si sa atterie
        if (!thrown && !hasLanded)
        {
            thrown = true;
            rb.useGravity = true;
            rb.AddTorque(Random.Range(0, 500), Random.Range(0, 500), Random.Range(0, 500));
        }
        else if (thrown && hasLanded) 
        {
        Reset();
        }
    }

    public void Reset()
    {

        transform.position = initPosition;
        thrown = false;
        hasLanded = false;
        rb.useGravity = false;
        rb.isKinematic = false;

    }
    public void RollAgain()
    {
        Reset();
        thrown = true;
        rb.useGravity = true;
        rb.AddTorque(Random.Range(0, 500), Random.Range(0, 500), Random.Range(0, 500));
    }

    public void SideValueCheck()
    {
        diceValue = 0;
        foreach (DiceSide side in diceSides)
        {
            if (side.getOnGround())
            {
                diceValue = side.sideValue;
                Debug.Log(diceValue + " has been rolled! ");
            }
        }
    }
}
