﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.EventSystems;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public List<Player> ListeJoueur = new List<Player>();

    [System.Serializable]
    //Joueur
    public class Player
    {
        public string nom;
        public GameObject myPion;
        public bool tour;
        public bool gagner;
    }

    //Quand le GameManager est en marche
    void Awake()
    {
        instance = this;
        foreach (Player joueur in instance.ListeJoueur)
        {
            joueur.myPion.gameObject.name = joueur.nom;
        }
    }
}
