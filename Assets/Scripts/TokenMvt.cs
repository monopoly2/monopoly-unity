﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class TokenMvt : MonoBehaviour
{
    public Route currentRoute;
    private int routePosition;
    private int routePosition2;
    private int routePosition3;
    private int routePosition4;
    private int steps;
    private bool isMoving;
    public DiceRoll[] dices;
    private int counter;
    private Vector3 nextPos;
    private bool rouler = true;
    private int[] commencement = new int[4];
    LecteurCase lc;

    // Démarre à chaque fois qu'on roule un des avec la touche espace
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isMoving)
        {
            counter = 0;
            steps = 0;

            for (int i = 0; i < dices.Length; i++)
            {
                if (dices[i].hasLanded == true && dices[i].thrown == true)
                {
                    steps += dices[i].diceValue;
                    counter++;
                }

                if (counter == dices.Length)
                {
                    counter++;
                }

                if (counter == dices.Length + 1)
                {
                    for (int h = 0; h < dices.Length; h++)
                    {
                        dices[h].Reset();
                    }
                }
            }

            if (rouler == true)
            {
                if (commencement[0] == 0)
                {
                    commencement[0] = steps;
                }
                else if (commencement[1] == 0 && commencement[0] != 0)
                {
                    commencement[1] = steps;
                }
                else if (commencement[2] == 0 && commencement[1] != 0)
                {
                    commencement[2] = steps;
                }
                else if (commencement[3] == 0 && commencement[2] != 0)
                {
                    commencement[3] = steps;
                    rouler = false;
                    place();
                }
            }
            else if (rouler == false)
            {
                Debug.Log("Commencement partie");
                ChangerJoueur();
            }
        }
    }

    public IEnumerator Move(GameObject pion)
    {
        lc = pion.GetComponent<LecteurCase>();
        if (isMoving)
        {
            yield break;
        }
        isMoving = true;

        if (GameManager.instance.ListeJoueur[0].tour == true)
        {
            // Bouger un nombre de case indiqué par le dée
            while (steps > 0)
            {
                routePosition++;
                routePosition %= currentRoute.nodeList.Count;
                if (routePosition == 0)
                {
                    lc.addSalary();
                }
                nextPos = currentRoute.nodeList[routePosition].position;
                while (MoveToNextNode(nextPos, pion)) { yield return null; }

                yield return new WaitForSeconds(0.1f);
                steps--;
            }
            isMoving = false;
            lc.actionFinder(routePosition);
        }
        else if (GameManager.instance.ListeJoueur[1].tour == true)
        {
            // Bouger un nombre de case indiqué par le dée
            while (steps > 0)
            {
                routePosition2++;
                routePosition2 %= currentRoute.nodeList.Count;
                if (routePosition2 == 0)
                {
                    lc.addSalary();
                }
                nextPos = currentRoute.nodeList[routePosition2].position;
                while (MoveToNextNode(nextPos, pion)) { yield return null; }

                yield return new WaitForSeconds(0.1f);
                steps--;
            }
            isMoving = false;
            lc.actionFinder(routePosition2);
        }
        else if (GameManager.instance.ListeJoueur[2].tour == true)
        {
            // Bouger un nombre de case indiqué par le dée
            while (steps > 0)
            {
                routePosition3++;
                routePosition3 %= currentRoute.nodeList.Count;
                if (routePosition3 == 0)
                {
                    lc.addSalary();
                }
                nextPos = currentRoute.nodeList[routePosition3].position;
                while (MoveToNextNode(nextPos, pion)) { yield return null; }

                yield return new WaitForSeconds(0.1f);
                steps--;
            }
            isMoving = false;
            lc.actionFinder(routePosition3);
        }
        else if (GameManager.instance.ListeJoueur[3].tour == true)
        {
            // Bouger un nombre de case indiqué par le dée
            while (steps > 0)
            {
                routePosition4++;
                routePosition4 %= currentRoute.nodeList.Count;
                if (routePosition4 == 0)
                {
                    lc.addSalary();
                }
                nextPos = currentRoute.nodeList[routePosition4].position;
                while (MoveToNextNode(nextPos, pion)) { yield return null; }

                yield return new WaitForSeconds(0.1f);
                steps--;
            }
            isMoving = false;
            lc.actionFinder(routePosition4);
        }
    }

    // Faire bouger le token au prochaine espace
    public bool MoveToNextNode(Vector3 goal, GameObject pion)
    {
        return goal != (pion.transform.position = Vector3.MoveTowards(pion.transform.position, goal, 8f * Time.deltaTime));
    }

    public void place()
    {
        int max = commencement.Max();

        if (commencement[0] == max)
        {
            GameManager.instance.ListeJoueur[0].tour = true;
        }
        else if (commencement[1] == max)
        {
            GameManager.instance.ListeJoueur[1].tour = true;
        }
        else if (commencement[2] == max)
        {
            GameManager.instance.ListeJoueur[2].tour = true;
        }
        else if (commencement[3] == max)
        {
            GameManager.instance.ListeJoueur[3].tour = true;
        }
    }

    public void ChangerJoueur()
    {
        Debug.Log(GameManager.instance.ListeJoueur[0].nom + " " + GameManager.instance.ListeJoueur[0].tour);
        Debug.Log(GameManager.instance.ListeJoueur[1].nom + " " + GameManager.instance.ListeJoueur[1].tour);
        Debug.Log(GameManager.instance.ListeJoueur[2].nom + " " + GameManager.instance.ListeJoueur[2].tour);
        Debug.Log(GameManager.instance.ListeJoueur[3].nom + " " + GameManager.instance.ListeJoueur[3].tour);

        if (GameManager.instance.ListeJoueur[0].tour == true)
        {
            StartCoroutine(Move(GameManager.instance.ListeJoueur[0].myPion));
            GameManager.instance.ListeJoueur[0].tour = false;
            GameManager.instance.ListeJoueur[1].tour = true;
        }
        else if (GameManager.instance.ListeJoueur[1].tour == true)
        {
            StartCoroutine(Move(GameManager.instance.ListeJoueur[1].myPion));
            GameManager.instance.ListeJoueur[1].tour = false;
            GameManager.instance.ListeJoueur[2].tour = true;
        }
        else if (GameManager.instance.ListeJoueur[2].tour == true)
        {
            StartCoroutine(Move(GameManager.instance.ListeJoueur[2].myPion));
            GameManager.instance.ListeJoueur[2].tour = false;
            GameManager.instance.ListeJoueur[3].tour = true;
        }
        else if (GameManager.instance.ListeJoueur[3].tour == true)
        {
            StartCoroutine(Move(GameManager.instance.ListeJoueur[3].myPion));
            GameManager.instance.ListeJoueur[3].tour = false;
            GameManager.instance.ListeJoueur[0].tour = true;
        }
    }
    public bool getIsMoving() 
    {
        return isMoving;
    }
}
