﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compte : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public string _nom;
    [SerializeField]
    public double _solde;
    [SerializeField]
    public double _pret;

    void Start()
    {
        _nom = gameObject.name;
        _solde = 1500;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void setNom(string nom)
    {
        this._nom = nom;

    }

    public string getNom()
    {

        string nom = "";
        nom = this._nom;
        return nom;

    }
    // une class qui permet d'ajouter de l'argent au compte du joueur
    public void ajouter(double largent)
    {
        this._solde += largent;

    }
    //elle permet de retirer de l'argent du compte du joueur
    //et la garder dans une variable pour ensuite la reetulisee
    public double retirer(double largent)
    {

        this._solde -= largent;

        return largent;

    }
    // verifier le solde du joueur 
    public double get_solde()
    {
        return this._solde;

    }
    // verifier les dettes du joueur
    public double get_pret()
    {
        return this._pret;

    }

    // faire un pret
    public void demander_pret(double pret)
    {
        this._pret += pret;
        this._solde += this._pret;


    }

    //permet au joueur d'eponger ses dettes au pret de la banque. 
    //le if verifie si le joueur a payer de trop sa dette et lui rembourse automatiquement le montant excedant
    public void rembouser_pret(double somme)
    {
        double excedent;
        if (somme > this._pret)
        {
            excedent = somme - this._pret;
            this._solde -= somme;
            this._solde += excedent;
            this._pret -= somme;
            this._pret -= excedent;

        }
        else
        {
            this._solde -= somme;
            this._pret -= somme;
        }
    }


}
