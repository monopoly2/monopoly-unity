﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Property : MonoBehaviour
{
    [SerializeField]
    private string propertyGroup;
    [SerializeField]
    private string propertyName;
    [SerializeField]
    private string description;
    [SerializeField]
    private int price;
    [SerializeField]
    private int mortgage;
    [SerializeField]
    private int[] rents = new int[7];
    [SerializeField]
    private int rent;
    [SerializeField]
    private string owner;
    [SerializeField]
    private int nbHouse;
    [SerializeField]
    private int hotel;
    [SerializeField]
    private int addPrice;
    [SerializeField]
    private int removePrice;
    [SerializeField]
    private bool monopoly;
    GameObject board;
    // Start is called before the first frame update
    void Start()
    {
        rent = rents[0];
        monopoly = false;
        board = GameObject.Find("BoardMonopoly");
        gameObject.name = propertyName;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public int getNbHouse()
    {
        return nbHouse;
    }
    public int getHotel()
    {
        return hotel;
    }
    public string getOwner()
    {
        return owner;
    }
    public void setOwner(string owner)
    {
        this.owner = owner;
    }
    public int[] getRents()
    {
        return rents;
    }
    public void setRents(int[] rents)
    {
        this.rents = rents;
    }
    public int getMortgage()
    {
        return mortgage;
    }
    public void setMortgage(int mortgage)
    {
        this.mortgage = mortgage;
    }
    public int getPrice()
    {
        return price;
    }
    public void setPrice(int price)
    {
        this.price = price;
    }
    public string getDescription()
    {
        return description;
    }
    public void setDescription(string description)
    {
        this.description = description;
    }
    public string getPropertyName()
    {
        return propertyName;
    }
    public void setPropertyName(string propertyName)
    {
        this.propertyName = propertyName;
    }
    public string getPropertyGroup()
    {
        return propertyGroup;
    }
    public void setPropertyGroup(string propertyGroup)
    {
        this.propertyGroup = propertyGroup;
    }
    public bool addHouse()
    {
        if (monopoly && nbHouse < 4 && hotel == 0)
        {
            nbHouse++;
            switch (nbHouse)
            {
                case 1:
                    setRent(rents[2]);
                    break;
                case 2:
                    setRent(rents[3]);
                    break;
                case 3:
                    setRent(rents[4]);
                    break;
                case 4:
                    setRent(rents[5]);
                    break;
            }
            return true;
        }
        return false;
    }
    public int getRemovePrice()
    {
        return removePrice;
    }
    public void setRemovePrice(int removePrice)
    {
        this.removePrice = removePrice;
    }
    public int getAddPrice()
    {
        return addPrice;
    }
    public void setAddPrice(int addPrice)
    {
        this.addPrice = addPrice;
    }
    public int getRent()
    {
        return rent;
    }
    public void setRent(int rent)
    {
        this.rent = rent;
    }
    public bool addHotel()
    {
        if (monopoly && nbHouse == 4 && hotel == 0)
        {
            hotel++;
            nbHouse = 0;
            setRent(rents[6]);
            return true;
        }
        return false;
    }
    public bool getMonopoly()
    {
        return monopoly;
    }
    public void setMonopoly(bool b)
    {
        monopoly = b;
    }
    public void verifyMonopoly()
    {
        int count = 0;
        ArrayList list = new ArrayList();
        foreach (Transform t in board.transform)
        {
            Property property = t.gameObject.GetComponent<Property>();
            if (property != null && property.getPropertyGroup().Equals(propertyGroup) && property.getOwner().Equals(owner))
            {
                count++;
                list.Add(property);
            }
        }
        if (count == 3 && !propertyGroup.Equals("DarkBlue") && !propertyGroup.Equals("Brown"))
        {
            foreach (Property pro in list)
            {
                pro.setRent(rents[1]);
                pro.setMonopoly(true);
            }
        }
        else
        {
            if (count == 2 && (propertyGroup.Equals("DarkBlue") || propertyGroup.Equals("Brown")))
            {
                foreach (Property pro in list)
                {
                    pro.setRent(rents[1]);
                    pro.setMonopoly(true);
                }
            }
            else
            {
                foreach (Property pro in list)
                {
                    pro.setMonopoly(false);
                    pro.setRent(rents[0]);
                }
            }
        }
    }
    public bool removeHotel()
    {
        if (hotel == 0)
        {
            return false;
        }
        else
        {
            hotel--;
            setRent(rents[5]);
            nbHouse = 4;
            return true;
        }
    }
    public bool removeHouse()
    {
        if (nbHouse == 0)
        {
            return false;
        }
        else
        {
            nbHouse--;
            switch (nbHouse)
            {
                case 1:
                    setRent(rents[2]);
                    break;
                case 2:
                    setRent(rents[3]);
                    break;
                case 3:
                    setRent(rents[4]);
                    break;
            }
            return true;
        }
    }
}

