﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool PauseJeu = false;

    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseJeu)
            {
                Continuer();
            } else
            {
                Pause();
            }
        }
    }

    public void Continuer ()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        PauseJeu = false;
    }

    void Pause ()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        PauseJeu = true;
    }

    public void LoadJeu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu Principale");
    }

    public void QuitterJeu()
    {
        Debug.Log("Quitter le jeu ...");
        Application.Quit();
    }
}
