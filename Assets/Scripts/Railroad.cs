﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Railroad : MonoBehaviour
{
    [SerializeField]
    private string propertyGroup;
    [SerializeField]
    private string propertyName;
    [SerializeField]
    private string description;
    [SerializeField]
    private int price;
    [SerializeField]
    private int mortgage;
    [SerializeField]
    private int[] rents = { 25, 50, 100, 200 };
    [SerializeField]
    private int rent;
    [SerializeField]
    private string owner;
    [SerializeField]
    private bool monopoly;
    GameObject board;
    // Start is called before the first frame update
    void Start()
    {
        propertyGroup = "Black";
        rent = rents[0];
        price = 200;
        mortgage = 100;
        board = GameObject.Find("BoardMonopoly");
        gameObject.name = propertyName;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public string getOwner()
    {
        return owner;
    }
    public void setOwner(string owner)
    {
        this.owner = owner;
    }
    public int[] getRents()
    {
        return rents;
    }
    public int getMortgage()
    {
        return mortgage;
    }
    public int getPrice()
    {
        return price;
    }
    public string getDescription()
    {
        return description;
    }
    public void setDescription(string description)
    {
        this.description = description;
    }
    public string getPropertyName()
    {
        return propertyName;
    }
    public void setPropertyName(string propertyName)
    {
        this.propertyName = propertyName;
    }
    public int getRent()
    {
        return rent;
    }
    public void setRent(int count)
    {
        if (count == 4)
        {
            rent = rents[3];
        }
        else if (count == 3)
        {
            rent = rents[2];
        }
        else if (count == 2)
        {
            rent = rents[1];
        }
        else
        {
            rent = rents[0];
        }
    }
    public string getPropertyGroup()
    {
        return propertyGroup;
    }
    public void setPropertyGroup(string propertyGroup)
    {
        this.propertyGroup = propertyGroup;
    }
    public void setMonopoly(bool b)
    {
        monopoly = b;
    }
    public void verifyMonopoly()
    {
        int count = 0;
        ArrayList list = new ArrayList();
        foreach (Transform t in board.transform)
        {
            Railroad railroad = t.gameObject.GetComponent<Railroad>();
            if (railroad != null && railroad.getOwner().Equals(owner))
            {
                count++;
                list.Add(railroad);
            }
        }
        if (count == 4)
        {
            foreach (Railroad rr in list)
            {
                rr.setRent(count);
                rr.setMonopoly(true);
            }
        }
        else
        {
            foreach (Railroad rr in list)
            {
                rr.setRent(count);
                rr.setMonopoly(false);
            }
        }
    }
}
