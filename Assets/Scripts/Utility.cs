﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    [SerializeField]
    private string propertyGroup;
    [SerializeField]
    private string propertyName;
    [SerializeField]
    private string description;
    [SerializeField]
    private int price;
    [SerializeField]
    private int mortgage;
    [SerializeField]
    private int rent;
    [SerializeField]
    private string owner;
    [SerializeField]
    private bool monopoly;
    [SerializeField]
    DiceRoll[] dices = new DiceRoll[2];
    [SerializeField]
    private int multiplier;
    GameObject board;
    // Start is called before the first frame update
    void Start()
    {
        propertyGroup = "LightGrey";
        price = 150;
        mortgage = 75;
        multiplier = 4;
        board = GameObject.Find("BoardMonopoly");
        gameObject.name = propertyName;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public string getOwner()
    {
        return owner;
    }
    public void setOwner(string owner)
    {
        this.owner = owner;
    }
    public int getMortgage()
    {
        return mortgage;
    }
    public int getPrice()
    {
        return price;
    }
    public string getDescription()
    {
        return description;
    }
    public void setDescription(string description)
    {
        this.description = description;
    }
    public string getPropertyName()
    {
        return propertyName;
    }
    public void setPropertyName(string propertyName)
    {
        this.propertyName = propertyName;
    }
    public int getRent()
    {
        setRent();
        return rent;
    }
    public void setRent()
    {
        rent = 0;
        foreach (DiceRoll dice in dices)
        {
            rent += dice.diceValue * multiplier;
        }
    }
    public string getPropertyGroup()
    {
        return propertyGroup;
    }
    public void setPropertyGroup(string propertyGroup)
    {
        this.propertyGroup = propertyGroup;
    }
    public bool getMonopoly()
    {
        return monopoly;
    }
    public void setMonopoly(bool b)
    {
        monopoly = b;
    }
    public void verifyMonopoly()
    {
        int count = 0;
        ArrayList list = new ArrayList();
        foreach (Transform t in board.transform)
        {
            Utility utility = t.gameObject.GetComponent<Utility>();
            if (utility != null && utility.getOwner().Equals(owner))
            {
                count++;
                list.Add(utility);
            }
        }
        if (count == 2)
        {
            foreach (Utility uti in list)
            {
                uti.setMultiplier(10);
                uti.setMonopoly(true);
            }
        }
        else
        {
            foreach (Utility uti in list)
            {
                uti.setMonopoly(false);
                uti.setMultiplier(4);
            }
        }
    }
    public int getMultiplier()
    {
        return multiplier;
    }
    public void setMultiplier(int multi)
    {
        multiplier = multi;
    }


}
