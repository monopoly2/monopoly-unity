﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LecteurCase : MonoBehaviour
{
    [SerializeField]
    private double montantBanque;
    [SerializeField]
    private int prixProperty;
    Compte joueur;
    GameObject findBoard;
    GameObject ChildGameObject1;
    int index;
    // Start is called before the first frame update
    void Start()
    {
        joueur = gameObject.GetComponent<Compte>();
        findBoard = GameObject.Find("BoardMonopoly");
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void addSalary()
    {
        joueur.ajouter(200);
        Debug.Log("Le joueur " + joueur.getNom() + " a gagné 200$ : " + joueur.get_solde());
    }
    public void actionFinder(int routePosition)
    {
        prixProperty = 0;
        double solde = joueur._solde;
        double vrai_s;
        ChildGameObject1 = findBoard.transform.GetChild(routePosition).gameObject;
        Property property = ChildGameObject1.GetComponent<Property>();
        Utility utility = ChildGameObject1.GetComponent<Utility>();
        Railroad railroad = ChildGameObject1.GetComponent<Railroad>();
        if (property)
        {
            if (!property.getOwner().Equals("") && !property.getOwner().Equals(joueur.getNom()))
            {
                prixProperty = property.getRent();
                Debug.Log(property.getPropertyName() + " appartient à " + property.getOwner());
                Debug.Log("Le prix de location de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    GameObject.Find(property.getOwner()).GetComponent<Compte>().ajouter(prixProperty);
                    vrai_s = joueur.get_solde();
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour payer le prix de location. Il est éliminé.");
                    GameObject.Find(property.getOwner()).GetComponent<Compte>().ajouter(joueur.get_solde());
                    Object.Destroy(gameObject);
                }
            }
            else if (property.getOwner().Equals(""))
            {
                prixProperty = property.getPrice();
                Debug.Log("Le prix d'achat de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    property.setOwner(joueur.getNom());
                    vrai_s = joueur.get_solde();
                    Debug.Log(joueur.getNom() + " est maintenant le propriétaire de " + property.getPropertyName());
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                    property.verifyMonopoly();
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour acheter cette propriété.");
                }
            }
            else if (property.getOwner().Equals(joueur.getNom()))
            {
                Debug.Log(joueur.getNom() + " est le propriétaire de la propriété : " + property.getPropertyName());
            }
        }
        else if (utility)
        {
            if (!utility.getOwner().Equals("") && !utility.getOwner().Equals(joueur.getNom()))
            {
                prixProperty = utility.getRent();
                Debug.Log(utility.getPropertyName() + " appartient à " + utility.getOwner());
                Debug.Log("Le prix de location de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    GameObject.Find(utility.getOwner()).GetComponent<Compte>().ajouter(prixProperty);
                    vrai_s = joueur.get_solde();
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour payer le prix de location. Il est éliminé.");
                    GameObject.Find(property.getOwner()).GetComponent<Compte>().ajouter(joueur.get_solde());
                    Object.Destroy(gameObject);
                }

            }
            else if (utility.getOwner().Equals(""))
            {
                prixProperty = utility.getPrice();
                Debug.Log("Le prix d'achat de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    utility.setOwner(joueur.getNom());
                    vrai_s = joueur.get_solde();
                    Debug.Log(joueur.getNom() + " est maintenant le propriétaire de " + utility.getPropertyName());
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                    utility.verifyMonopoly();
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour acheter cette propriété.");
                }
            }
            else if (utility.getOwner().Equals(joueur.getNom()))
            {
                Debug.Log(joueur.getNom() + " est le propriétaire de la propriété : " + utility.getPropertyName());
            }
        }
        else if (railroad)
        {
            if (!railroad.getOwner().Equals("") && !railroad.getOwner().Equals(joueur.getNom()))
            {
                prixProperty = railroad.getRent();
                Debug.Log(railroad.getPropertyName() + " appartient à " + railroad.getOwner());
                Debug.Log("Le prix de location de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    GameObject.Find(railroad.getOwner()).GetComponent<Compte>().ajouter(prixProperty);
                    vrai_s = joueur.get_solde();
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour payer le prix de location. Il est éliminé.");
                    GameObject.Find(property.getOwner()).GetComponent<Compte>().ajouter(joueur.get_solde());
                    Object.Destroy(gameObject);
                }
            }
            else if (railroad.getOwner().Equals(""))
            {
                prixProperty = railroad.getPrice();
                Debug.Log("Le prix d'achat de cette propriété est de : " + prixProperty);
                Debug.Log("Votre solde est de : " + (solde));
                if (joueur.get_solde() >= prixProperty)
                {
                    joueur.retirer(prixProperty);
                    railroad.setOwner(joueur.getNom());
                    vrai_s = joueur.get_solde();
                    Debug.Log(joueur.getNom() + " est maintenant le propriétaire de " + railroad.getPropertyName());
                    Debug.Log("Le montant restant est de : " + (vrai_s));
                    Debug.Log(joueur.getNom() + " a fait un retrait de : " + (prixProperty));
                    railroad.verifyMonopoly();
                }
                else
                {
                    Debug.Log(joueur.getNom() + " n'a pas assez d'argent pour acheter cette propriété.");
                }

            }
            else if (railroad.getOwner().Equals(joueur.getNom()))
            {
                Debug.Log(joueur.getNom() + " est le propriétaire de la propriété : " + railroad.getPropertyName());
            }
        }
    }

}