﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour {

    // Liste de type Transform(Permet de manipuler un position , un rotation et un scale)
    public Transform[] childObjects;
    public List<Transform> nodeList = new List<Transform>();

    // Methode qui permet de visualiser la route
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        FillNodes();

        for (int i = 0; i < nodeList.Count; i++)
        {
            Vector3 currentPos = nodeList[i].position;
            if (i > 0) {
                Vector3 prevPos = nodeList[i - 1].position;
                Gizmos.DrawLine(prevPos, currentPos);
            }
        }
    }

    // Prend tout les GameObjects enfant et l'ajout a la liste automatiquement 
    public void FillNodes() {
        nodeList.Clear();

        childObjects = GetComponentsInChildren<Transform>();

        foreach (Transform child in childObjects) {
            if (child != this.transform) {
                nodeList.Add(child);
            }
        }

    }
}
